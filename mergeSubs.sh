#!/bin/bash

function infoMkv() {
    # Listando tracks de archivo .mkv
    FILE=$1
    mkvmerge -i "$FILE"
}

function delSubMkv() {
    # Borrando subtitulos
    FILE=$1
    mkvmerge -o "$FILE"".tmp" \
        --no-subtitles \
        "$FILE"
    
    mv -f "$FILE"".tmp" "$FILE"
}

function addSubMkv() {
    # Agrega subtitulo a .mkv
    TARGET_LANGUAGE='spa'
    FILE=$1
    SUBTITLE=$2

    echo "$FILE"".tmp" " -> " "$FILE" " -> " "$SUBTITLE"

    mkvmerge -o "$FILE"".tmp" \
        "$FILE" \
        "$SUBTITLE" \
        --chapter-language $TARGET_LANGUAGE

    mv -f "$FILE"".tmp" "$FILE"
}

SOURCE_FILE=$1
SUBTITLE_FILE=$2

infoMkv "$SOURCE_FILE"

delSubMkv "$SOURCE_FILE"

addSubMkv "$SOURCE_FILE" "$SUBTITLE_FILE"

infoMkv "$SOURCE_FILE"
