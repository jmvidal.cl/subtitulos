#!/bin/bash

FILTRO='.srt'
LIST_FILE='./listado.lst'

PATH_FILE=$1

find "$1" -type f -exec echo "{}" \; | grep $FILTRO > $LIST_FILE

while read -r FILE
do

    bash translateSub.sh "$FILE"

done < "$LIST_FILE" 
