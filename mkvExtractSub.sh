#!/bin/bash

FILTRO='.mkv'
SOURCE_SUB_EXT='.pgs'
STAGE_SUB_EXT='.sub'
TARGET_SUB_EXT='.srt'
LIST_FILE='./listado.lst'

PATH_FILE=$1
TRACK_SUBTITLE=4

echo "Procesando $PATH_FILE ..."
PATH_FILE=${PATH_FILE//\\//}

find "$1" -type f -exec echo "{}" \; | grep $FILTRO > $LIST_FILE

while read -r FILE
do
    NEW_FILE=${FILE// /\\ }
    NEW_FILE=$(basename -s "$FILTRO" "$NEW_FILE")
    echo $NEW_FILE

    mkvmerge -i "$FILE"
    # echo "Seleccione track de subtitulo..."
    # read TRACK

    # mkvextract tracks "$FILE" $TRACK_SUBTITLE:"$NEW_FILE$SOURCE_SUB_EXT"
    mkvextract tracks "$FILE" $TRACK_SUBTITLE:"$NEW_FILE$TARGET_SUB_EXT"
    # java -jar /usr/share/bdsup2sub/BDSup2Sub.jar "$NEW_FILE$SOURCE_SUB_EXT" "$NEW_FILE$STAGE_SUB_EXT" 

    # vobsub2srt --lang en "$NEW_FILE"

done < "$LIST_FILE" 
