#!/bin/bash

SOURCE_FILE=$1
SOURCE_LANG='en'
TARGET_LANG='es'
EXTENSION="${SOURCE_FILE##*.}"
STAGE_FILE="${SOURCE_FILE%.*}""_"$SOURCE_LANG"."$EXTENSION
TARGET_FILE="${SOURCE_FILE%.*}""_"$TARGET_LANG"."$EXTENSION

declare -a ENGINE
SELECT_ENGINE=1
ENGINE_LIST=(aspell google bing spell hunspell apertium yandex)
ENGINE=${ENGINE_LIST[$SELECT_ENGINE]}

echo $ENGINE " -> " $SOURCE_LANG " -> " $TARGET_LANG " -> " $TARGET_FILE

REGEX_POS="^[0-9]+$"
REGEX_TIME="^([0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9])"
REGEX_END="^\s*$"

# SECCION=''
echo '' > "$STAGE_FILE"
while read -r LINE
do

    if [[ $LINE =~ $REGEX_POS ]]
    then
        # echo $LINE
        # SECCION=''
        SECCION=$LINE
    else
        if [[ $LINE =~ $REGEX_TIME ]]
        then 
            # echo $LINE
            SECCION=$SECCION"||"$LINE
        else
            if [[ $LINE =~ $REGEX_END ]]
            then 
                echo $SECCION >> "$STAGE_FILE"
            else
                SECCION=$SECCION"||"$LINE
            fi
        fi
    fi

done < "$SOURCE_FILE"

echo '' > "$TARGET_FILE"

while read -r LINE
do
    FLAG=true
    while $FLAG
    do
        TRANS_LINE=$(trans -engine $ENGINE -brief -s $SOURCE_LANG -t $TARGET_LANG "$LINE")
        # echo ${#TRANS_LINE} " - " ${#LINE}

        if [[ ${#LINE} -gt 0 ]]
        then
            if [[ ${#TRANS_LINE} -gt 0 ]]
            then
                FLAG=false
            fi
        else
            FLAG=false
        fi
        # echo $FLAG
    done

    POS=$(echo $LINE | awk '{print $1}')

    # echo -n $POS
    printf '\u2584'

    echo $TRANS_LINE >> "$TARGET_FILE"
    
done < "$STAGE_FILE"

