#!/bin/bash

declare -A MATRIX

FILTRO='.srt'
LIST_FILE='./listado.lst'

PATH_FILE=$1
PATH_BASE='/home/jmvidal'

MATRIX[1,1]=": "
MATRIX[2,1]=" -> "
MATRIX[3,1]="dieciséis"
MATRIX[4,1]="sesenta y cinco"

MATRIX[1,2]=":"
MATRIX[2,2]=" --> "
MATRIX[3,2]="16"
MATRIX[4,2]="65"

echo "Procesando $PATH_FILE ..."
PATH_FILE=${PATH_FILE//\\//}

find "$1" -type f -exec echo "{}" \; | grep $FILTRO > $LIST_FILE

while read -r FILE
do
    FILE_FIX=${FILE// /\\ }
    FILE_FIX=$(basename "$FILE_FIX")
    for ((i=1; i<=4; i++)) do
        SRCH=${MATRIX[$i,1]}
        REPL=${MATRIX[$i,2]}
        BEFORE=$((i-1))
        if [[ $i -eq 1 ]]; then
            SOURCE_FILE=$FILE
            TARGET_FILE=$FILE.$i
        else
            SOURCE_FILE=$FILE.$BEFORE
            TARGET_FILE=$FILE.$i
        fi
        PARAM="s/$SRCH/$REPL/g"
        echo $FILE
        sed "$PARAM" "$SOURCE_FILE" > "$TARGET_FILE"
        if [[ $i -ne 1 ]]; then
            rm "$FILE.$BEFORE"
        fi
    done
    cat "$TARGET_FILE" > "$FILE"
    rm "$TARGET_FILE"
done < "$LIST_FILE" 
