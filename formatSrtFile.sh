#!/bin/bash

SOURCE_FILE=$1
TARGET_FILE=$1".srt"

REGEX_POS="^[0-9]+$"
REGEX_END="^\s*$"
IFS='||'

echo '' > "$TARGET_FILE"
while read -r LINE
do
    read -ra ADDR <<< "$LINE" 
    for SECCION in "${ADDR[@]}"
    do
        if [[ ${#SECCION} -gt 0 ]]
        then
            if [[ $SECCION =~ $REGEX_POS ]]
            then 
                echo '' >> "$TARGET_FILE"
            fi
            echo $SECCION >> "$TARGET_FILE"
        fi
    done

done < "$SOURCE_FILE"
